import math

TO_GOAL = "TO_GOAL"
GOAL = "GOAL"

class Model(object):
    '''A Model is essentially a weighted deterministic finite-state
    automaton over the alphabet {(span, ewords)}.'''
    
    def __init__(self):
        object.__init__(self)

        # Initial state
        self.initial = None

        # Decoder hints
        self.stateless = 0 # state is always None, and estimate_transition is in fact exact.
        self.wordless = 0  # transition is independent of ewords
    
    def input(self, fwords):
        '''Allows a model to initialize itself depending on the input.
        
        fwords is a list of strings
        '''
        
        return None
    
    def transition (self, covered, state, transition):
        '''Return new state after transition and cost of transition.'''

        return (state, 0.0)


    def estimate_rule (self, r):
        '''Return an estimate based on rule identity alone'''
        return 0.0


    def estimate_transition (self, transition):
        '''Estimate the cost, independent of state, of a transition.
        If stateless is true, then this will get used as the real
        cost.'''
        
        return 0.0

    def estimate_future (self, covered, state):
        """Estimate the cost of getting from a state to the final
        state. This gets added to the cost of some future path
        according to estimate_transition, so don't double-count."""
        
        return 0.0

    def generate_spans (self, covered, state):
        '''Generate French spans for a given state.'''

        return []

    def generate_ephrases (self, span):
        '''Generate English phrases corresponding to span.'''

        return []
        
class DistortionModel(Model):
    # could only penalize rightward distortions, since the leftward
    # distortions end up with the same total anyway
    def __init__(self, min=0, max=None, master=True):
        Model.__init__(self)
        self.n = None
        self.alpha = math.log10(math.e)
        self.escape_hatch = 1
        self.stateless = 0
        self.wordless = 1
        self.initial = 0

        self.min=min
        self.max=max
        self.master=master

    def input(self, fwords):
        self.n = len(fwords)

    def transition (self, covered, last_j, transition):
        #((i,j), ewords, pm_scores) = transition
        if transition is TO_GOAL:
            jump = self.n-last_j
            state = self.n
        else:
            (i,j) = transition[0]
            jump = abs(i-last_j)
            state = j

        if jump >= self.min and (self.max is None or jump <= self.max):
            return (state, self.alpha*jump)
        else:
            return (state, 0.0)

    def estimate_future (self, covered, last_j):
        # cost of getting back to first uncovered, then to the end
        # before the first hole, incur nothing
        # from the first hole to last_j, incur alpha per uncovered, 2*alpha per covered
        # after last_j, incur alpha per covered

        if self.master:
            return self.alpha*(abs(last_j-covered.first_uncovered)+covered.n_covered-covered.first_uncovered)
        else:
            return 0.0

    def generate_spans (self, covered, last_j):
        if not self.master:
            return
        (min_i, max_i) = (0, self.n-1)
        if self.min is not None:
            min_i = max(0, last_j+self.min)
        if self.max is not None:
            max_i = min(self.n-1, last_j+self.max)
        irange = range(min_i, max_i+1)
        
        # If option set, include the first uncovered French word for
        # consideration even if outside of distortion limits. This
        # helps repair "Swiss cheese" hypotheses.

        # Pharaoh doesn't do this. Instead it has some kind of
        # additional hard limit.  For a limit of 4, *____> is allowed
        # but _*____> is not.  We should ask him what it is.  Guess:
        # When I add a phrase, make sure I can jump from the end of
        # the phrase to the beginning of the last hole. If I also
        # created a new hole, make sure I can jump from the end of the
        # hole to the beginning of the previous.

        if self.escape_hatch and not min_i <= covered.first_uncovered <= max_i:
            irange += [covered.first_uncovered]

        # loop through holes
        for i in irange:
            for j in xrange(i+1,self.n+1):
                if covered.c[j-1]:
                    break
                yield (i,j)

class WordPenalty(Model):
    def __init__(self):
        Model.__init__(self)
        self.omega = math.log10(math.e)
        self.stateless = 1

    def transition (self, covered, state, transition):
        if transition is not TO_GOAL:
            ewords = transition[1]
            return (None, self.omega * len(ewords))
        else:
            return (None, 0.0)

    def estimate_rule(self, r):
        return self.omega * len(r.e)

    def estimate_transition (self, transition):
        ewords = transition[1]
        return self.omega * len(ewords)
